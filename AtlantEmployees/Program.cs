﻿using System;
using System.Windows.Forms;
using AtlantEmployees.Forms;

namespace AtlantEmployees
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new InboxForm());
        }
    }
}
