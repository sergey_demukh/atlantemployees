﻿using AtlantEmployees.Models;
using AtlantEmployees.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AtlantEmployees.Forms
{
    public partial class InboxForm : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=.\SQL;Integrated Security=True");
        public InboxForm()
        {
            InitializeComponent();
        }

        private void InboxWindow_Load(object sender, EventArgs e)
        {
            var dbProvider = new DataBaseProvider();
            dataGridView1.DataSource = dbProvider.GetStudentInfo();

            DeleteBatton.Width = 50;
            ReadMoreBatton.Width = 50;
            EditBatton.Width = 50;
            
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["ReadMoreBatton"].Index)
            {
                //Do Something with your button.
                var selectedStudent = (StudentInfo)dataGridView1.CurrentRow.DataBoundItem;
                var aboutForm = new AboutStudentForm(selectedStudent.Id);
                aboutForm.Show();
            }

            if (e.ColumnIndex == dataGridView1.Columns["EditBatton"].Index)
            {
                var selectedStudent = (StudentInfo)dataGridView1.CurrentRow.DataBoundItem;
                var efitForm = new EditStudent(selectedStudent.Id);
                efitForm.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
                efitForm.Show();
            }

            if (e.ColumnIndex == dataGridView1.Columns["DeleteBatton"].Index)
            {
                var selectedStudent = (StudentInfo)dataGridView1.CurrentRow.DataBoundItem;
                var deleteForm = new DeleteStudentForm(selectedStudent.Id);
                deleteForm.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
                deleteForm.Show();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var a = new AddStudentForm();
            a.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            a.Show();
        }
        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Do something
            var dbProvider = new DataBaseProvider();
            dataGridView1.DataSource = dbProvider.GetStudentInfo();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            var dbProvider = new DataBaseProvider();
            dataGridView1.DataSource = dbProvider.GetStudentInfo();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            connection.Open();
            var a = textBox1.Text;
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select s.ID as Id, " +
                    "s.[FirstName] + ' ' + s.[SecondName] + ' ' + s.[LastName] as [FIO], " +
                    "f.[Name] as [Faculty], " +
                    "k.[Name] as [Speciality], " +
                    "g.[Name] as [Group] " +
                    "from Students.dbo.Faculties f " +
                    "inner join Students.dbo.Students s on s.FacultyID = f.Id " +
                    "inner join Students.dbo.Specialities k on s.SpecialityID = k.ID " +
                    "inner join Students.dbo.Groups g on s.GroupID = g.ID " +
                    "where s.FirstName like ('%" + a + "%')";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            connection.Close();
        }

 
    }

    }
















