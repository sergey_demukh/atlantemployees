﻿using AtlantEmployees.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtlantEmployees.Forms
{
    public partial class DeleteStudentForm : Form
    {
        private int Id;
        DataBaseProvider dbProvider; 

        public DeleteStudentForm(int ID)
        {
            InitializeComponent();
            Id = ID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dbProvider = new DataBaseProvider();
            var result = dbProvider.DeleteStudentInfo(Id);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
