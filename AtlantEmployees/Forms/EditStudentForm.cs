﻿using AtlantEmployees.Models;
using AtlantEmployees.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AtlantEmployees.Forms
{
    public partial class EditStudent : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=.\SQL;Integrated Security=True");
        private int Id;
        DataBaseProvider dbProvider;

        public EditStudent(int studId)
        {
            InitializeComponent();
            Id = studId;
        }
        
        private void EditStudentForm_Load(object sender, EventArgs e)
        {
            dbProvider = new DataBaseProvider();
            var resul = dbProvider.GetEditStudent(Id);
            textBox11.Text = resul.FirstName;
            textBox12.Text = resul.SecondName;
            textBox13.Text = resul.LastName;
            textBox21.Text = resul.City;
            textBox22.Text = resul.Street;
            textBox23.Text = resul.House;
            textBox24.Text = Convert.ToString(resul.PostalCode);

            comboBox1.DataSource = dbProvider.GetGroups();
            comboBox1.DisplayMember = "Name";
            comboBox1.SelectedIndex = -1;

            comboBox2.DataSource = dbProvider.GetFaculties();
            comboBox2.DisplayMember = "Name";
            comboBox2.ValueMember = "Id";
            comboBox2.SelectedIndex = -1;

            comboBox3.DataSource = dbProvider.GetSpecialities();
            comboBox3.DisplayMember = "Name";
            comboBox3.ValueMember = "Id";
            comboBox3.SelectedIndex = -1;

            comboBox4.DataSource = dbProvider.GetCountries();
            comboBox4.DisplayMember = "Name";
            comboBox4.ValueMember = "Id";
            comboBox4.SelectedIndex = -1;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd = new SqlCommand(@"UPDATE Students.dbo.Students SET FirstName = '" + textBox11.Text +
                "' ,[SecondName] = '" + textBox12.Text + 
                "', [LastName] = '" + textBox13.Text + "', [Birthday] = '" + dateTimePicker3.Value +
                "', [DateIN] = '" + dateTimePicker1.Value + "', " +
                "[DateOUT] = '" + dateTimePicker2.Value +
                "', [GroupID] = '1', "+
                "[FacultyID] = '" + comboBox2.SelectedValue +
                "', [SpecialityID] = '" + comboBox3.SelectedValue + "' where Id= '" + Id + "' ", connection);
            cmd.ExecuteNonQuery();
           /* SqlCommand adr = new SqlCommand(@"UPDATE Students.dbo.Adresses SET CountryID = '" + comboBox4.SelectedValue +
                "', City ='" + textBox4.Text + "', Street = '" + textBox8.Text + "', House = '" + textBox9.Text + "', PostalCode = '" + textBox10 +"' "+
                "where StudID = '"+Id+"' ", connection);
            
            adr.ExecuteNonQuery();*/
            connection.Close();
            this.Close();
        }

        
    }
}

