﻿using AtlantEmployees.Models;
using AtlantEmployees.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtlantEmployees.Forms
{
    public partial class AddStudentForm : Form
    {
        DataBaseProvider dbProvider;

        public AddStudentForm()
        {
            InitializeComponent();
        }

        private void AddWindowForm_Load(object sender, EventArgs e)
        {
            dbProvider = new DataBaseProvider();
            // get group collection from DB as groups
            // get faculties collection as faculties
            // get specialities collection as specialities
            //get countries collection as countries
                

            comboBox1.DataSource = dbProvider.GetGroups();
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";
            comboBox1.SelectedIndex = -1;

            comboBox2.DataSource = dbProvider.GetFaculties();
            comboBox2.DisplayMember = "Name";
            comboBox2.ValueMember = "Id";
            comboBox2.SelectedIndex = -1;

            comboBox3.DataSource = dbProvider.GetSpecialities();
            comboBox3.DisplayMember = "Name";
            comboBox3.ValueMember = "Id";
            comboBox3.SelectedIndex = -1;

            comboBox4.DataSource = dbProvider.GetCountries();
            comboBox4.DisplayMember = "Name";
            comboBox4.ValueMember = "Id";
            comboBox4.SelectedIndex = -1;

        }

        private void button1_Click(object sender, EventArgs e)
        {   
            Student student = new Student {
                FirstName = textBox1.Text,
                LastName = textBox3.Text,
                SecondName= textBox2.Text,
                FacultyId = (int)comboBox2.SelectedValue,
                SpecialityId = (int)comboBox3.SelectedValue,
                GroupId = (int)comboBox1.SelectedValue,
                DateIn = (DateTime)dateTimePicker1.Value,
                DateOut =(DateTime)dateTimePicker2.Value,
                Birthday =(DateTime)dateTimePicker3.Value
            };

            var id = dbProvider.SaveStudent(student);
            Adress adresses = new Adress
            {
                CountryId = (int)comboBox4.SelectedValue,
                City= textBox4.Text,
                Street = textBox8.Text,
                House = textBox9.Text,
                PostalCode = textBox10.Text,
                SudentId = id
            };
            dbProvider.SaveAdressStudent(adresses);
            this.Close();
            //save new student
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //close form
            this.Close();
        }

        

        
    }
}
