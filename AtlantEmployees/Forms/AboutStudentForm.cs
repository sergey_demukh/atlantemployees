﻿using AtlantEmployees.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtlantEmployees.Forms
{
    public partial class AboutStudentForm : Form
    {
        DataBaseProvider dbProvider;
        private int id;

        public AboutStudentForm(int studentId)
        {
            InitializeComponent();
            id = studentId;
        }

        private void ReedMoreWindowsForm_Load(object sender, EventArgs e)
        {
            dbProvider = new DataBaseProvider();
           var result= dbProvider.GetAboutStudent(id);
            textBox14.Text = result.FIO;
            textBox16.Text = result.Group;
            textBox17.Text = result.Faculty;
            textBox18.Text = result.Speciality;
            textBox20.Text = result.Country;
            textBox21.Text = result.City;
            textBox22.Text = result.Street;
            textBox23.Text = result.House;
            textBox24.Text = Convert.ToString(result.PostalCode);
        }

        public static explicit operator AboutStudentForm(DataGridViewButtonColumn v)
        {
            throw new NotImplementedException();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }
    }
}