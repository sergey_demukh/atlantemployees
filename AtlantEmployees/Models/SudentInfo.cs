﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtlantEmployees.Models
{
    public class  StudentInfo
    {
        public string FIO { get; set; }
        public string Faculty { get; set;}
        public string Speciality { get; set; }
        public string Group { get; set; }
        public int Id { get; set; }
    }
}
