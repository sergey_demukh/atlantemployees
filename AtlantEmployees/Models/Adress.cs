﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtlantEmployees.Models
{
    public class Adress
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int SudentId { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string PostalCode { get; set; }
    }
}
