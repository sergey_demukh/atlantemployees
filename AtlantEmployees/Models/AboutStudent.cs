﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtlantEmployees.Models
{
    public class AboutStudent
    {
        public string FIO { get; set; }
        public string Faculty { get; set; }
        public string Speciality { get; set; }
        public string Group { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string House { get; set; }
        public string Street { get; set; }
        public int PostalCode { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
    }
}
