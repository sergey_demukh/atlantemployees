﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtlantEmployees.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondName { get; set; }
        public int FacultyId { get; set; }
        public int SpecialityId { get; set; }
        public int GroupId { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        public int CountryId { get; set; }
    }
}
