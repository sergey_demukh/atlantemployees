﻿using AtlantEmployees.Models;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System;
using AtlantEmployees.Forms;

namespace AtlantEmployees.Services
{
    public class DataBaseProvider
    {
        private object connection;
        private IDbTransaction transaction;
        private object subCategory;

        private IDbConnection OpenConnection()
        {
            var connetionString = @"Data Source=.\SQL;Integrated Security=True";
            var connection = new SqlConnection(connetionString);
            connection.Open();
            return connection;
        }

        public List<Group> GetGroups()
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "SELECT [ID] as Id, [Name] FROM [Students].[dbo].[Groups]";
                var result = (connection.Query<Group>(query));
                var groups = new List<Group>(result);
                return groups;
            }
        }

        

        public List<Faculty> GetFaculties()
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "SELECT [Id],[Name] FROM[Students].[dbo].[Faculties]";
                var result = (connection.Query<Faculty>(query));
                var faculties = new List<Faculty>(result);
                return faculties;
            }
        }

        public List<Speciality> GetSpecialities()
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "SELECT [ID] as Id,[Name] FROM[Students].[dbo].[Specialities]";
                var result = (connection.Query<Speciality>(query));
                var specialities = new List<Speciality>(result);
                return specialities;
            }
        }

        public List<Country> GetCountries()
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "SELECT [ID] as Id,[Name] FROM[Students].[dbo].[Countries]";
                var result = (connection.Query<Country>(query));
                var countries = new List<Country>(result);
                return countries;
            }
        }
        public List<StudentInfo> GetStudentInfo()
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "Select s.ID as Id, " +
                    "s.[FirstName] + ' ' + s.[SecondName] + ' ' + s.[LastName] as [FIO], " +
                    "f.[Name] as [Faculty], " +
                    "k.[Name] as [Speciality], " +
                    "g.[Name] as [Group] " +
                    "from Students.dbo.Faculties f " +
                    "inner join Students.dbo.Students s on s.FacultyID = f.Id " +
                    "inner join Students.dbo.Specialities k on s.SpecialityID = k.ID " +
                    "inner join Students.dbo.Groups g on s.GroupID = g.ID";
                var result = (connection.Query<StudentInfo>(query));
                var student = new List<StudentInfo>(result);
                return student;
            }
        }

        public AboutStudent GetAboutStudent(int Id)
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "Select s.ID as Id, [FirstName] + ' ' + [SecondName] + ' ' + [LastName] as [FIO], " +
                    "f.[Name] as [Faculty], " +
                    "k.[Name] as [Speciality], " +
                    "g.[Name] as [Group], " +
                    "c.[Name] as [Country], " +
                    "a.[City],a.[Street],a.[House],a.[PostalCode] " +
                    "from Students.dbo.Students s " +
                    "inner join Students.dbo.Faculties f on s.FacultyID = f.Id " +
                    "inner join Students.dbo.Specialities k on s.SpecialityID = k.ID " +
                    "inner join Students.dbo.Groups g on s.GroupID = g.ID " +
                    "left join Students.dbo.Adresses a on s.Id = a.StudID " +
                    "left join Students.dbo.Countries c on c.Id = a.CountryID " +
                    "where s.Id=@Id";
                return connection.Query<AboutStudent>(query, new { Id = Id }).SingleOrDefault();
            }
        }

        public int DeleteStudentInfo(int Id)
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "DELETE FROM Students.dbo.Adresses WHERE StudID = @Id Delete from Students.dbo.Students where ID = @Id";
                int rowsAffected = connection.Execute(query, new { ID = Id }, transaction);
                //transaction.Commit();
                return rowsAffected;
            }
        }

        public int SaveStudent(Student student)
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = " INSERT INTO Students.dbo.Students (s.FirstName,s.[SecondName],s.[LastName], " +
                   "s.[Birthday],s.[GroupID],s.[FacultyID],s.[SpecialityID],s.[DateIN],s.[DateOUT]) " +
                    "VALUES (@FirstName, @SecondName, @LastName, @Birthday, @GroupId, @FacultyId, @SpecialityId, @DateIN, @DateOUT)";
                var parametrs = new
                {
                    FirstName = student.FirstName,
                    SecondName = student.SecondName,
                    LastName = student.LastName,
                    Birthday=student.Birthday,
                    GroupId = student.GroupId,
                    FacultyId = student.FacultyId,
                    SpecialityId = student.SpecialityId,
                    DateIN = student.DateIn,
                    DateOut=student.DateOut
                };
                connection.Execute(query, parametrs);
                dynamic identity = connection.Query("SELECT @@IDENTITY AS Id").Single();
                int newId = (int)identity.Id;
                return newId;
            }
        }
        public void SaveAdressStudent(Adress adresses)
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "INSERT INTO Students.dbo.Adresses " +
                "(CountryID, City, Street, House, PostalCode, StudID) " +
                "VALUES (@CountryID,@City, @Street, @House , @PostalCode, @StudentID) ";
                var parametrs = new
                {
                    CountryID = adresses.CountryId,
                    City = adresses.City,
                    Street = adresses.Street,
                    House = adresses.House,
                    PostalCode = adresses.PostalCode,
                    StudentID = adresses.SudentId
                };
                int rowsAffectd = connection.Execute(query, parametrs);
            }
        }
        protected static void SetIdentity<T>(IDbConnection connection, Action<T> setId)
        {
            dynamic identity = connection.Query("SELECT @@IDENTITY AS Id").Single();
            T newId = (T)identity.Id;
            setId(newId);
        }
        public AboutStudent GetEditStudent(int Id)
        {
            using (IDbConnection connection = OpenConnection())
            {
                const string query = "Select s.ID as Id, s.[FirstName], s.[SecondName],  s.[LastName], s.Birthday, s.DateIN, s.DateOUT, " +
                    "s.Birthday , f.[Name] as [Faculty], " +
                    "k.[Name] as [Speciality], " +
                    "g.[Name] as [Group], " +
                    "c.[Name] as [Country], " +
                    "a.[City],a.[Street],a.[House],a.[PostalCode] " +
                    "from Students.dbo.Students s " +
                    "inner join Students.dbo.Faculties f on s.FacultyID = f.Id " +
                    "inner join Students.dbo.Specialities k on s.SpecialityID = k.ID " +
                    "inner join Students.dbo.Groups g on s.GroupID = g.ID " +
                    "left join Students.dbo.Adresses a on s.Id = a.StudID " +
                    "left join Students.dbo.Countries c on c.Id = a.CountryID " +
                    "where s.Id=@Id";
                return connection.Query<AboutStudent>(query, new { Id = Id }).SingleOrDefault();
            }
        }
       

    }
}
