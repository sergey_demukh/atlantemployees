﻿using AtlantEmployees.Models;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace TestDapper
{
    class Program
    {
        private static IDbConnection OpenConnection()
        {
            var connetionString = @"Data Source=.\SQL;Integrated Security=True";
            var connection = new SqlConnection(connetionString);
            connection.Open();
            return connection;
        }

        static void Main(string[] args)
        {

            using (IDbConnection connection = OpenConnection())
            {
                const string query = "SELECT [ID] as Id, [Name] FROM [Students].[dbo].[Groups]";
                var groups = connection.Query<Group>(query);
            }
        }
    }
}
